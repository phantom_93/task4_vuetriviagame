import Vue from 'vue'
import VueRouter from 'vue-router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import StartScreen from '@/components/StartScreen'
import TriviaScreen from '@/components/TriviaScreen'
import ResultScreen from '@/components/ResultScreen'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter); 
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

export default new VueRouter({
  // setting up routes 
    routes: [

        {
            path:'/',
            redirect:'/start'
        },

        {
            path:'/start',
            name: 'StartScreen',
            component: StartScreen
        },

        {
            path:'/trivia',
            name: 'TriviaScreen',
            component: TriviaScreen
        },


        {
            path:'/results',
            name: 'ResultScreen',
            component: ResultScreen
        }
    ]
});