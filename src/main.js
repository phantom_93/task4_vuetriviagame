import Vue from 'vue'
import App from './App.vue'
import router from './router/view' 

Vue.config.productionTip = false

new Vue({
  router, // added router 
  render: h => h(App),
}).$mount('#app')
