// fetch the questions from the API 
/* 
   Number of Questions: 10
   Select Category: Science
   Select Difficult: easy
   Select Type: True / False
   Select Encoding: Default Encoding 
*/
let questions = []; 

export const getAllQuestions = () =>{
    questions = fetch("https://opentdb.com/api.php?amount=10&category=18&difficulty=easy&type=boolean")
    .then(response => response.json())
    .then(data => data.results);
    return questions; 
}



